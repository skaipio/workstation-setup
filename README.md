# Automated Linux (Debian) workstation setup with Ansible

## Prerequisites
[Setup Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installation-guide) on your target machine.

## Running the Ansible playbook

Copy `user_variables.example.yaml` to `user_variables.yaml` and configure the variables to your liking.

Some parts of the playbook require sudo privileges. The `-K` option is set to ask you for your password to be able to run these tasks.

Run the following to set up your system.
```shell
ansible-playbook -K playbook.yaml
```

To skip font installation, for example if you're setting up WSL, run:
```shell
ansible-playbook -K --skip-tags font playbook.yaml
```

Files in `./cache` can be removed. They are created again when running the playbook.
